import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, login: 'admin', name: 'Administrator', password: 'Pass@1234 ' },
  { id: 2, login: 'user1', name: 'User 1', password: 'Pass@1234 ' },
  { id: 3, login: 'user2', name: 'User 2', password: 'Pass@1234 ' },
];

let lastProductId = 4;
@Injectable()
export class ProductService {
  create(createUserDto: CreateProductDto) {
    console.log({ ...CreateProductDto });
    const newProduct: products = {
      id: lastProductId++,
      ...createUserDto, // login name pass
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((user) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('user ' + JSON.stringify(users[index]));
    // console.log('update ' + JSON.stringify(updateUserDto));
    const updateProduct: Product = {
      ...products[index],
      ...updateProduct,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct = products[index];
    products.splice(index, 1);
    return deletedProduct;
  }

  reset() {
    products = [
      { id: 1, login: 'admin', name: 'Administrator', password: 'Pass@1234 ' },
      { id: 2, login: 'user1', name: 'User 1', password: 'Pass@1234 ' },
      { id: 3, login: 'user2', name: 'User 2', password: 'Pass@1234 ' },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
